require("../src/db/mongoose");
const User = require("../src/models/user");

// id:- 620604d8c3d9e3324907173c

// User.findByIdAndUpdate("620604f3c3d9e3324907173e", { age: 24 }).then((user) => {
//     console.log(user);
//     return User.countDocuments({ age: 24 })
// }).then((result) => {
//     console.log(result);
// }).catch((err) => {
//     console.log(err);
// })

const updateAgeAndCount = async (id, age) => {
    const user = await User.findByIdAndUpdate(id, { age })
    const count = await User.countDocuments({ age })
    return count
}

updateAgeAndCount("620604f3c3d9e3324907173e", 2).then((count) => {
    console.log(count);
}).catch((err) => {
    console.log(err);
})