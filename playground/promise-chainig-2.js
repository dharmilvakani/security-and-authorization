require("../src/db/mongoose");
const Tasks = require("../src/models/tasks");

// Tasks.findByIdAndDelete("62061cb6bc7138f0e65a00bd", {

// }).then((task) => {
//     console.log(task);
//     return Tasks.countDocuments({ completed: false })
// }).then((res) => {
//     console.log(res);
// }).catch((err) => {
//     console.log(err);
// })


const deleteTaskAndCount = async (id) => {
    await Tasks.findByIdAndDelete(id);
    const count = await Tasks.countDocuments({ completed: false });
    return count;
}

deleteTaskAndCount("6206078566e7a4bf82dd9e5a").then((count) => {
    console.log(count);
}).catch((err) => {
    console.log(err);
})