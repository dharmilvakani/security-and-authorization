const User = require('../models/user');

const createUser = async (req, res) => {
    const user = new User(req.body);
    try {
        await user.save();
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }
}

const loginUser = async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({ user, token });
    } catch (error) {
        res.status(400).send()
    }
}

const logoutUser = async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token
        })
        await req.user.save();
        res.send();
    } catch (error) {
        res.status(500).send()
    }
}

const logoutAll = async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();
        res.send()
    } catch (error) {
        res.status(500).send()
    }
}

const getMe = async (req, res) => {
    res.send(req.user)
}

const updateUser = async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdate = ['name', 'email', 'password', 'age'];
    const validOperation = updates.every((update) => allowedUpdate.includes(update));
    if (!validOperation) {
        res.status(400).send({ error: "Invalid Updates" })
    }
    try {
        updates.forEach((update) => req.user[update] = req.body[update]);
        await req.user.save();
        res.status(200).send(req.user);
    } catch (error) {
        res.status(400).send()
    }
}

const deleteUser = async (req, res) => {
    try {
        await req.user.remove()
        res.send(req.user)
    } catch (error) {
        res.status(400).send({ error: "Not found" })
    }
}

module.exports = { createUser, loginUser, logoutUser, logoutAll, getMe, updateUser, deleteUser }